'use strict';

// check configs here:
// https://github.com/timgit/pg-boss/wiki/configuration
module.exports = {
  user: '',
  password: '',
  host: 'localhost',
  port: '5432',
  database: '',
  schema: '',
  uuid: 'v4',
  newJobCheckInterval: '200',
  expireCheckIntervalSeconds: '10',
  archiveCompletedJobsEvery: '30 minutes',
  archiveCheckIntervalMinutes: '30',
};
