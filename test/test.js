'use strict';

const PgBossWrapper = require('../index.js');
const express = require('express');

const config = require('./testConfigMe');

let pgBossWrapper;

function doSmth() {
  console.log('doSmth');
  pgBossWrapper.addJob('test1', {}, {
    expireIn: '1 minutes',
    priority: '102',
    // singletonKey: 'xyz',
  })
    .then((jobId) => {
      console.log('job test1 added', jobId);
    })
    .catch((error) => {
      console.error('job test1 add failed', error);
    });
}

const initConfig = {
  debug: true,
  config,
  engine: 'worker',
  workerOptions: {
    teamSize: 100,
  },
  queueName: 'work-ocean',
  onReady: function onReady() {
    console.log('on ready');
    // setInterval(() => {
    //   doSmth();
    // }, 500);
  },
  onError: function onError(error) {
    console.error('on error', error);
  },
  onArchived: function onArchived(count) {
    console.warn('on archived', count);
  },
  onExpired: function onExpired(count) {
    console.warn('on expired', count);
  },
  onJob: function onJob(name, payload, done) {
    // console.log('on job', { name, payload });
    // TODO do something
    // console.log(new Date(), 'job executing ... 2 sec');
    setTimeout(() => {
      console.log(new Date(), 'job executed', name);
      done();
    }, 2000);
  },
  onFailed: function onFailed(failure) {
    console.error('on failed', failure);
  },
  onMonitorStates: function onMonitorStates(monitorStates) {
    if (monitorStates.created > 100) {
      console.warn('pg-boss: has more than 100 created, so maybe there is a bottle neck somewhere?');
    }
    console.log('monitorStates', JSON.stringify(monitorStates));
  },
};

pgBossWrapper = new PgBossWrapper(initConfig);

const app = express();

app.get('/postJob', (req, res) => {
  doSmth();
  res.sendStatus(200);
});
app.get('/stop', (req, res) => {
  console.log(pgBossWrapper.boss.disconnect());
  res.sendStatus(200);
});

app.listen(3000, () => console.log('Test app listening on port 3000!'));

