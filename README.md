# README #

This is a wrapper for the [pg-boss](https://github.com/timgit/pg-boss) library for scheduling/processing jobs with 
a queue in PostgreSQL.

---

## Prepare IDE

### Install eslint:

```
npm -g install eslint eslint-plugin-import eslint-config-airbnb-base
```

### Intellij: 

WebStorm -> Preferences -> Languages & Frameworks -> Javascript -> Code Quality Tools -> Eslint -> tick Enable

import javascript code style: `dvel_webstorm_formatting.xml`

**Always reformat your code before you commit!** You can create a macro which reformats and saves on a cmd+s.

Be sure to tick these checkmarks in the commit window (then you cant forget to reformat):

![Screenshot_2017-01-25_11_21_10-4.png](https://bitbucket.org/repo/ydGXXd/images/3594142576-Screenshot_2017-01-25_11_21_10-4.png)

---

# Work Flow

API (**master** with queuer) -> schedules job into Postgres -> Worker (**slave**) grabs & process it.

I tested it with multiple masters and multiple slaves -> success

# Deployment

You need a PostgreSQL with the schema or if your user has permission to create schemas and tables it gets created on its own.

Look at [heroes-postgres](https://bitbucket.org/dvelinc/heroes-postgres) to setup the database correctly.

# Push new version

Use one of the following commands to up the version and push it to the remote git.
It also merge into `master`, so be careful when you push a new version.
```
npm run release-patch 
# 1.0.0 => 1.0.1
npm run release-minor 
# 1.0.1 => 1.1.1
npm run release-major 
# 1.1.0 => 2.1.1
```