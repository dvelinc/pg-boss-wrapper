'use strict';

const PgBoss = require('pg-boss');
const Promise = require('bluebird');

const defaultQueueName = 'work';
const defaultPgBossConfig = {
  poolSize: 20, // max pool size
  uuid: 'v4',
  newJobCheckInterval: '1000',
  expireCheckIntervalSeconds: '10',
  archiveCompletedJobsEvery: '1 minute',
  archiveCheckIntervalMinutes: 5,
  deleteArchivedJobsEvery: '3 days',
  deleteCheckInterval: 60 * 60 * 1000, // 1 hour
  // monitorStateIntervalSeconds: '5', // should not be activated by default
};
const defaultJobOptions = {
  startIn: 0,
  retryLimit: 2,
  expireIn: '1 minute',
  priority: 0,
};
const defaultWorkerOptions = {
  teamSize: 10,
};
const master = 'master';
const scheduler = 'scheduler';
const worker = 'worker';

function PgBossWrapper(options) {
  const that = this;

  const engine = options.engine;
  const debug = !!options.debug;

  that.onReady = options.onReady;
  that.onError = options.onError;
  that.onArchived = options.onArchived;
  that.onExpired = options.onExpired;
  that.onJob = options.onJob;
  that.onFailed = options.onFailed;
  that.onMonitorStates = options.onMonitorStates;

  that.queueName = options.queueName || defaultQueueName;

  const boss = new PgBoss(Object.assign({}, defaultPgBossConfig, options.config));
  that.boss = boss;

  boss.on('error', (error) => {
    let newError = null;
    if (error.message.includes('this version of pg-boss does not appear to be installed') ||
      error.message.includes('permission denied for database')) {
      newError = 'Seems like you have not installed pg-boss in the database or there is a permission problem!\n' +
        'Use the following script to create the tables and check the permissions:' +
        `\n${PgBoss.getConstructionPlans(options.config.schema)}`;
    }
    if (that.onError && typeof that.onError === 'function') {
      that.onError(error);
      if (newError) {
        that.onError(newError);
      }
    }
  });
  boss.on('archived', (count) => {
    if (that.onArchived && typeof that.onArchived === 'function') {
      that.onArchived(count);
    }
  });
  boss.on('expired-count', (count) => {
    if (that.onExpired && typeof that.onExpired === 'function') {
      that.onExpired(count);
    }
  });
  boss.on('failed', (failure) => {
    if (that.onFailed && typeof that.onFailed === 'function') {
      that.onFailed(failure);
    }
  });
  boss.on('monitor-states', (monitorStates) => {
    if (that.onMonitorStates && typeof that.onMonitorStates === 'function') {
      that.onMonitorStates(monitorStates);
    }
  });

  function initMaster() {
    return boss.start();
  }

  function initScheduler() {
    return boss.connect();
  }

  function initWorker() {
    if (!that.onJob || typeof that.onJob !== 'function') {
      throw new Error('PgBossWrapper.initWorker: for the "worker" engine, you have to define the options.onJob()');
    }
    return boss.connect()
      .then(() => {
        boss.subscribe(that.queueName, options.workerOptions || defaultWorkerOptions, (job, done) => {
          const internalJobName = job.data.internalJobName;
          delete job.data.internalJobName;
          job.data.internalJobId = job.id;
          that.onJob(internalJobName, job.data, done);
        });
      });
  }

  function startEngine(shouldStart = true) {
    if (!shouldStart) {
      return Promise.resolve();
    }
    switch (engine) {
      case master:
        return initMaster();
      case scheduler:
        return initScheduler();
      case worker:
        return initWorker();
      default:
        throw new Error('engine is not supported');
    }
  }

  function start(shouldStart = true) {
    return startEngine(shouldStart)
      .then(() => {
        if (that.onReady && typeof that.onReady === 'function') {
          that.onReady();
        }
      })
      .catch((error) => {
        boss.emit('error', error);
        return Promise.reject(error);
      });
  }

  start();

  /**
   * addJob
   * ATTENTION: if you use date objects in the payload it will be a string after you receive the job!
   * @param name string
   * @param _payload object if you want to add infos for your job
   * @param addJobOptions object { startIn, retryLimit, expireIn, priority }
   *   startIn, int: seconds to delay starting the job; default: 0
   *   retryLimit, int: how often to retry; default: 2
   *   expireIn, string: PostgreSQL interval: if a job fails and is still set to 'active' then retry after x time;
   *     default '1 minute'
   *   priority, int: 0=lowest; 0>highest
   *   singletonKey, string: only creates one job with this key
   *   more infos on options: https://github.com/timgit/pg-boss/wiki/Configuration#publish-options
   *
   * @returns Promise jobId string
   */
  that.addJob = function addJob(name, _payload, addJobOptions) {
    let payload = _payload;
    return Promise.resolve()
      .then(() => {
        if (debug) {
          console.log('PgBossWrapper:addJob:', { name, payload });
        }
        if (!name) {
          throw new Error('PgBossWrapper.addJob: name parameter missing');
        }
        if (payload && typeof payload !== 'object') {
          throw new Error('PgBossWrapper.addJob: payload parameter must be an object');
        }
        if (options && typeof options !== 'object') {
          throw new Error('PgBossWrapper.addJob: options parameter must be an object');
        }
        if (!payload) {
          payload = {};
        }
        payload.internalJobName = name; // will be used later in the worker
        // merge default options with custom options
        const jobOptions = Object.assign({}, defaultJobOptions, addJobOptions);

        // isStarting has to be true/false & isReady has to be undefined, its a state where
        // { isStarting: false, isReady: undefined, isStarted: undefined }
        // boss exited the starting state and next tick is ready and started
        if (boss.isStarting !== undefined && boss.isReady === undefined) {
          return Promise.delay(500)
            .then(() => {
              return that.addJob(name, _payload, addJobOptions);
            });
        }
        return boss.publish(that.queueName, payload, jobOptions)
          .catch((error) => {
            if (error.includes('boss ain\'t ready.')) {
              // boss is not started, start it & retry =>
              return Promise.delay(500)
                .then(start)
                .then(() => that.addJob(name, _payload, addJobOptions));
            }
            throw new Error(error);
          });
      });
  };
}

/**
 * get other configs from here:
 * https://github.com/timgit/pg-boss/wiki/configuration
 * @param options object {
 *   config: {
 *     host: 'localhost',
 *     port: '5432',
 *     database: '',
 *     schema: '',
 *     user: '',
 *     password: '',
 *     newJobCheckInterval: '1000',
 *     expireCheckIntervalSeconds: '10',
 *   },
 *   engine: 'master' possible values: master, scheduler, worker
 *     'master':    is able to add jobs and handles all the archive and expire stuff => keeps database clean
 *     'scheduler': is able to add jobs
 *     'worker':    is able to receive jobs (and add jobs)
 *   workerOptions: { teamSize: 3 } how many workers will listen concurrently; default: 3
 *   queueName: 'work' (optional) any non-space string to use different queus in the same database table.
 *     master and worker must have the same queueName to write/read from the same job queue
 *   onReady:     function()      get called when initialized
 *   onError:     function(error) get called on error
 *   onArchived:  function(count) get called after jobs got archived, count of archived
 *   onExpired:   function(count) get called after jobs got expired, count of expired
 *   onJob:       function(name, payload, done) if its a 'worker' engine, this function gets called on receiving a job
 *     the payload includes the 'internalJobId' => job id in postgres
 *   onFailed:    function(failure) get called when job failed
 *   onMonitorStates:    function(monitorStates) get called when monitor event occurs -> useful for debugging
 *                       only enabled if you set `monitorStateIntervalSeconds`; remember: need performance on database
 * }
 * @returns {PgBossWrapper}
 */
module.exports = function init(options) {
  if (!options) {
    throw new Error('PgBossWrapper.init: options parameter missing');
  }
  if (!options.config) {
    throw new Error('PgBossWrapper.init: options.config parameter missing');
  }
  if (!options.engine || ![master, scheduler, worker].includes(options.engine)) {
    throw new Error('PgBossWrapper.init: options.engine parameter is missing or not valid, ' +
      'possible values: master, job, worker');
  }
  return new PgBossWrapper(options || {});
};
/**
 * PgBoss: pg-boss module exported
 */
module.exports.PgBoss = PgBoss;
